package main

import (
	"github.com/gin-gonic/gin"
	"fmt"
	"os/exec"
	"strings"
)

var DB = make(map[string]string)

func check(e error) {
	if e!= nil {
		panic(e)
	}
}

func setupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()
	r.Static("/pdf", "/home/stef/pdfs")
	r.Static("/docx", "./docx")

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		f, err := exec.Command("ls /").Output()
		if err != nil {
			fmt.Println(err.Error())
		}
		fmt.Println(string(f))
		c.String(200, "pong")
	})

	r.GET("/convert", func(c *gin.Context) {
		target_url := c.Query("target_url")
		cmd := exec.Command("wkhtmltopdf", target_url, "/home/stef/pdfs/"+target_url+".pdf")
		err := cmd.Start()
		if err != nil {
			fmt.Println(err)
		}
		c.String(200, target_url)
	})

	r.POST("/html2docx", func(c *gin.Context) {
		file, _ := c.FormFile("file")
		_split := strings.Split(file.Filename, ".")
		file_name := _split[0]
		c.SaveUploadedFile(file, "./html/"+file.Filename)
		cmd := exec.Command("mono", "./html2docx/bin/Release/html2docx.exe", "./html/"+file.Filename, "./docx/"+file_name+".docx");
		err := cmd.Start()
		if err != nil {
			fmt.Println(err)
		}
		c.JSON(200, gin.H{"error": 0, "file_name": file_name+".docx"});
	})

//
//	r.GET("/pdf2txt", func(c *gin.Context) {
//		pdf := c.Query("pdf")
//		res, err := docconv.ConvertPath("/home/stef/pdfs"+pdf)
//		if err != nil {
//			fmt.Println(string(err))
//		}
//		c.String(200, string(res))
//	})

	r.GET("/download", func(c *gin.Context) {
		target_pdf := c.Query("target_pdf")
		c.Redirect(302, "/pdf/"+target_pdf+".pdf")
	})

	// Get user value
	r.GET("/user/:name", func(c *gin.Context) {
		user := c.Params.ByName("name")
		value, ok := DB[user]
		if ok {
			c.JSON(200, gin.H{"user": user, "value": value})
		} else {
			c.JSON(200, gin.H{"user": user, "status": "no value"})
		}
	})

	// Authorized group (uses gin.BasicAuth() middleware)
	// Same than:
	// authorized := r.Group("/")
	// authorized.Use(gin.BasicAuth(gin.Credentials{
	//	  "foo":  "bar",
	//	  "manu": "123",
	//}))
	authorized := r.Group("/", gin.BasicAuth(gin.Accounts{
		"foo":  "bar", // user:foo password:bar
		"manu": "123", // user:manu password:123
	}))

	authorized.POST("admin", func(c *gin.Context) {
		user := c.MustGet(gin.AuthUserKey).(string)

		// Parse JSON
		var json struct {
			Value string `json:"value" binding:"required"`
		}

		if c.Bind(&json) == nil {
			DB[user] = json.Value
			c.JSON(200, gin.H{"status": "ok"})
		}
	})

	return r
}

func main() {
	r := setupRouter()
	// Listen and Server in 0.0.0.0:8080
	r.Run(":8080")
}
