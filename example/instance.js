const fs = require('fs');
const request = require('request');

//upload file
let formData = {
    file: fs.createReadStream('/home/stef/pdfs/convert.html')
}

request.post({
    url: "http://localhost:1314/html2docx",
    formData: formData
}, function(err, resp, body){
    console.log(err || body)
    //download file
    let file_name = JSON.parse(body).file_name;
    request('http://localhost:1314/docx/'+file_name).pipe(fs.createWriteStream('/home/stef/desktop/ins.docx'));
})

